<?php
class ModelPaymentPAYLINE extends Model {
  	public function getMethod() {
		$this->load->language('payment/payline');

		if ($this->config->get('payline_status')) {
      		  	$status = TRUE;
      	} else {
			$status = FALSE;
		}
		
		$method_data = array();
	
		if ($status) {  
      		$method_data = array( 
			    'terms'      => '',
        		'code'       => 'payline',
        		'title'      => $this->language->get('text_title')."<br/><img src='catalog/view/theme/default/image/payline.gif'/>",
				'sort_order' => $this->config->get('payline_sort_order')
      		);
    	}
   
    	return $method_data;
  	}
}
?>