<?php
require_once(DIR_SYSTEM.'library/payline_func/sender.php');
class ControllerPaymentPayline extends Controller {
	public function index() {
		$this->load->language('payment/payline');

		$data['text_instruction'] = $this->language->get('text_instruction');
		$data['text_description'] = $this->language->get('text_description');
		$data['text_payment'] = $this->language->get('text_payment');

		$data['button_confirm'] = $this->language->get('button_confirm');

		$data['bank'] = nl2br($this->config->get('payline_bank' . $this->config->get('config_language_id')));

		$data['continue'] = $this->url->link('checkout/success');

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/payment/payline.tpl')) {
			return $this->load->view($this->config->get('config_template') . '/template/payment/payline.tpl', $data);
		} else {
			return $this->load->view('default/template/payment/payline.tpl', $data);
		}
	}
public function confirm() {

		$this->load->model('checkout/order');

		$order_info = $this->model_checkout_order->getOrder($this->session->data['order_id']);

		//$this->load->library('encryption');

		//$encryption = new Encryption($this->config->get('config_encryption'));

		//$data['Amount'] = $this->currency->format($order_info['total'], 'TMN', $order_info['value'], FALSE);


		$data['Amount'] = $this->currency->format($order_info['total'], $order_info['currency_code'], $order_info['currency_value'], false);

		//$data['Amount']=$data['Amount']\10;


		$data['PIN']=$this->config->get('payline_PIN');


		$data['ResNum'] = $this->session->data['order_id'];

		$data['return'] = $this->url->link('checkout/success', '', 'SSL');
		//$data['return'] = HTTPS_SERVER . 'index.php?route=checkout/success';

		$data['cancel_return'] = $this->url->link('checkout/payment', '', 'SSL');
		//$data['cancel_return'] = HTTPS_SERVER . 'index.php?route=checkout/payment';

		$data['back'] = $this->url->link('checkout/payment', '', 'SSL');

        $url = 'http://payline.ir/payment/gateway-send';

		$amount = $data['Amount'];
		if($this->currency->getCode()!='RLS') {
		    $amount=$amount * 10;
	    }

        //$data['order_id'] = $encryption->encrypt($this->session->data['order_id']);
        $data['order_id'] = $this->encryption->encrypt($this->session->data['order_id']);

		$callbackUrl  =  urlencode($this->url->link('payment/payline/callback', 'order_id=' . $data['order_id'], 'SSL'));

        $result = send($url,$data['PIN'],$amount,$callbackUrl);
        if($result < 0){
            $json = array();
	    	$json['error']= "Can not connect to Payline.<br>";

		    $this->response->setOutput(json_encode($json));
        }


		if($result > 0 && is_numeric($result)){

		$data['action'] = "http://payline.ir/payment/gateway-$result";
		$json = array();
		$json['success']= $data['action'];

		$this->response->setOutput(json_encode($json));

		} else {

			$this->CheckState($result);
			//die();
		}

//



}

	public function CheckState($status) {
		$json = array();

	switch($status){

		case "-1" :
			$json['error']="api ارسالی با نوع api تعریف شده در payline سازگار نیست.";
			break;
		case "-2" :
			$json['error']="مقدار amount داده عددی نمی باشد.";
			break;
		case "-3" :
		 $json['error']="مقدار redirect رشته null است";
			break;
		case "-4" :
			$json['error']="درگاهی با اطلاعات ارسالی شما یافت نشده و یا در حالت انتظار می باشد.";
			break;
		default :
			$json['error']= "خطای نامشخص;";
			break;
	}


		$this->response->setOutput(json_encode($json));

}

function verify_payment($trans_id, $get_id){

    $data['PIN'] = $this->config->get('payline_PIN');
    $url = 'http://payline.ir/payment/gateway-result-second';
    $result = get($url,$data['PIN'],$trans_id,$get_id);
	$this->CheckState($result);

	if($result==1)
		return true;

	else {
		return false;
	}

	}


	public function callback() {
		//$this->load->library('encryption');

		//$encryption = new Encryption($this->config->get('config_encryption'));
        $trans_id = $_POST['trans_id'];
        $id_get = $_POST['id_get'];
		$order_id = $this->encryption->decrypt($this->request->get['order_id']);
		$MerchantID=$this->config->get('payline_PIN');
		$debugmod=false;

		$this->load->model('checkout/order');
		$order_info = $this->model_checkout_order->getOrder($order_id);


			$Amount = $this->currency->format($order_info['total'], $order_info['currency_code'], $order_info['currency_value'], false);		//echo $data['Amount'];
		$amount = $Amount/$order_info['currency_value'];

		if ($order_info) {
			if(($this->verify_payment($trans_id, $id_get)) or ($debugmod==true)) {
			$this->model_checkout_order->addOrderHistory($order_id, $this->config->get('payline_order_status_id'),'شماره رسيد ديجيتالي; Authority: '.$trans_id);

				$this->response->setOutput('<html><head><meta http-equiv="refresh" CONTENT="2; url=' . $this->url->link('checkout/success') . '"></head><body><table border="0" width="100%"><tr><td>&nbsp;</td><td style="border: 1px solid gray; font-family: tahoma; font-size: 14px; direction: rtl; text-align: right;">با تشکر پرداخت تکمیل شد.لطفا چند لحظه صبر کنید و یا  <a href="' . $this->url->link('checkout/success') . '"><b>اینجا کلیک نمایید</b></a></td><td>&nbsp;</td></tr></table></body></html>');
			}else{
                $this->response->setOutput('<html><body><table border="0" width="100%"><tr><td>&nbsp;</td><td style="border: 1px solid gray; font-family: tahoma; font-size: 14px; direction: rtl; text-align: right;">.<br />خريد انجام نشد<br /><a href="' . $this->url->link('checkout/cart').  '"><b>بازگشت به فروشگاه</b></a></td><td>&nbsp;</td></tr></table></body></html>');
            }
		} else {
			$this->response->setOutput('<html><body><table border="0" width="100%"><tr><td>&nbsp;</td><td style="border: 1px solid gray; font-family: tahoma; font-size: 14px; direction: rtl; text-align: right;">.<br /><br /><a href="' . $this->url->link('checkout/cart').  '"><b>بازگشت به فروشگاه</b></a></td><td>&nbsp;</td></tr></table></body></html>');
		}
	}

}
?>
